/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package server_bingo;

import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.*;
/**
 *
 * @author gianpieromode
 */
public class CardGenerator {
    
    private int [][][] card  = new int[500][500][500];//{{{0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}}};
    public CardGenerator(){
        int i = 0;
        int j = 0;
        int k = 0;
        while(i < 100){
            while(j < 100){
                while(k < 100){
                    card[i][j][k]=0;
                    k++;
                }
                j++;
            }
            i++;
        }
    }
    public void Generate(int max, int quantity, int size){
        int carton = 0;
        Random randomNumber = new Random();
        
        while(carton < quantity){
            int fila = 0;
            ArrayList<Integer> usedNumbers = new ArrayList<Integer>();
            usedNumbers.add(0);
            int[][] temporaryCard = {{0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}};
            while(fila < size){
                int columna = 0;
                while(columna < size){
                    int number = randomNumber.nextInt(max);

                    while(usedNumbers.contains(number)){
                        
                        number = randomNumber.nextInt(max);
                    }
                    temporaryCard[fila][columna] = number;
                    usedNumbers.add(number);
                    columna++;
                }
                columna = 0;
                fila++;
            }

            //printCard(temporaryCard);
            int [][] emptyCard = new int[0][0];
            if(verifyCard(carton,temporaryCard)){
                card[carton] = temporaryCard;
                carton++;
            }
            temporaryCard = emptyCard;
            fila =0;
        }
        SaveCards(1, card[1], size);
        //Cards will be save here
    }
    public void SaveCards(int number, int [][] card, int size){
                Conector conection = new Conector();
                conection.InsertCard(number, card, size);
    } 
    public boolean verifyCard(int numberOfCards, int[][] temporaryCard){
        int i = 0;
        boolean verification = true;
        while(i < numberOfCards){
            if(Arrays.equals(card[i],temporaryCard)){
                verification = verification & false;
            }
            i++;
        }
        return verification;
    }
    public void printCard(int card[][]){
        for(int fila = 0; fila < 5; fila ++){
            for(int columna = 0; columna < 5; columna ++){
                System.out.print(card[fila][columna] + "  ");
            }
            System.out.println();
        }
    }

}
